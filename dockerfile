# Stages definition
stages:
  - build
  - test
  - dockerize

# build - Build sources
build:
  stage: build
  image: node:20-alpine
  before_script:
    - npm install
  script:
    - echo "Start building App"
    - npm run build
    - echo "Build done !"
  artifacts:
    expire_in: 15 minutes
    paths:
      - build/
      - node_modules/

# test - Run Tests
test:
  stage: test
  image: node:20-alpine
  script:
    - echo "Testing App"
    - npm install
    - npm test
    - echo "Test done !"


dockerize:
  stage: dockerize
  image:
    name: gcr.io/kaniko-project/executor:v1.14.0-debug
    entrypoint: [""]
  script:
    - /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      --destination "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}"
      --build-arg "NODEJS_VERSION=${NODEJS_VERSION}"
  variables:
    NODEJS_VERSION: 20
  rules: 
  - if: $CI_COMMIT_BRANCH == "main"
